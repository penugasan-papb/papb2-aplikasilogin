package com.example.rendscoffee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.bck2
import kotlinx.android.synthetic.main.activity_login.btn2l
import kotlinx.android.synthetic.main.activity_login.nreg1
import kotlinx.android.synthetic.main.activity_opening.btn1
import kotlinx.android.synthetic.main.activity_opening.btn2
import kotlinx.android.synthetic.main.activity_register.bck1
import kotlinx.android.synthetic.main.activity_register.nreg2

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)


        btn2l.setOnClickListener {
            startActivity(Intent(this, MainActivity::class.java))
        }

        bck2.setOnClickListener {
            startActivity(Intent(this, Opening::class.java))

        }

        nreg1.setOnClickListener {
            startActivity(Intent(this, Register::class.java))

        }

    }
}