package com.example.rendscoffee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.bck3
import kotlinx.android.synthetic.main.activity_main.dine1
import kotlinx.android.synthetic.main.activity_main.takehome1
import kotlinx.android.synthetic.main.activity_register.bck1

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bck3.setOnClickListener {
            startActivity(Intent(this, Opening::class.java))
        }

        dine1.setOnClickListener {
            startActivity(Intent(this, Opening::class.java))
        }

        takehome1.setOnClickListener {
            startActivity(Intent(this, Opening::class.java))
        }
    }
}