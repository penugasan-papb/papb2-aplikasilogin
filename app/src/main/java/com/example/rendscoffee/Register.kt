package com.example.rendscoffee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_login.btn2l
import kotlinx.android.synthetic.main.activity_login.nreg1
import kotlinx.android.synthetic.main.activity_register.bck1
import kotlinx.android.synthetic.main.activity_register.btn2r
import kotlinx.android.synthetic.main.activity_register.nreg2


class Register : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        btn2r.setOnClickListener{
            startActivity(Intent(this, Login::class.java))
        }

        bck1.setOnClickListener{
            startActivity(Intent(this, Opening::class.java))
        }

        nreg2.setOnClickListener{
            startActivity(Intent(this, Login::class.java))
        }

        }

    }
