package com.example.rendscoffee

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_opening.btn1
import kotlinx.android.synthetic.main.activity_opening.btn2

class Opening : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_opening)

        btn1.setOnClickListener{
            startActivity(Intent(this, Login::class.java))
        }

        btn2.setOnClickListener{
            startActivity(Intent(this, Register::class.java))
        }

    }
}